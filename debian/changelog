libsyntax-keyword-dynamically-perl (0.11-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 06 Apr 2023 12:13:07 +0000

libsyntax-keyword-dynamically-perl (0.11-1) unstable; urgency=medium

  * Import upstream version 0.11.
  * New build dependency: libfuture-asyncawait-perl >= 0.60.
  * Refresh hardening.patch (offset).
  * Declare compliance with Debian Policy 4.6.1.
  * One more test dependency for one more test.

 -- gregor herrmann <gregoa@debian.org>  Fri, 25 Nov 2022 17:46:40 +0100

libsyntax-keyword-dynamically-perl (0.10-1) unstable; urgency=medium

  * Import upstream version 0.10.
  * Update years of upstream and packaging copyright.
  * Make build dependency on libdevel-mat-dumper-perl versioned,
    and remove <!nocheck>, it's used from Build.PL.

 -- gregor herrmann <gregoa@debian.org>  Sun, 20 Mar 2022 21:18:04 +0100

libsyntax-keyword-dynamically-perl (0.09-1) unstable; urgency=medium

  * Import upstream version 0.09.
  * New (build) dependency: libxs-parse-keyword-perl.
  * Refresh hardening.patch (offset).
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Wed, 29 Sep 2021 16:01:17 +0200

libsyntax-keyword-dynamically-perl (0.07-1apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Thu, 22 Apr 2021 15:24:11 +0200

libsyntax-keyword-dynamically-perl (0.07-1) unstable; urgency=medium

  * Import upstream version 0.07.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.5.1.

 -- gregor herrmann <gregoa@debian.org>  Fri, 05 Feb 2021 00:31:57 +0100

libsyntax-keyword-dynamically-perl (0.06-1) unstable; urgency=medium

  * Import upstream version 0.06.

 -- gregor herrmann <gregoa@debian.org>  Tue, 03 Nov 2020 21:56:17 +0100

libsyntax-keyword-dynamically-perl (0.05-1) unstable; urgency=medium

  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.
  * Add libdevel-mat-dumper-perl to Build-Depends.
  * Import upstream version 0.05.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Fri, 10 Jul 2020 18:10:05 +0200

libsyntax-keyword-dynamically-perl (0.04-1) unstable; urgency=medium

  * Import upstream version 0.04.

 -- gregor herrmann <gregoa@debian.org>  Sat, 07 Mar 2020 13:58:32 +0100

libsyntax-keyword-dynamically-perl (0.03-1) unstable; urgency=medium

  * Import upstream version 0.03.
  * Update years of upstream and packaging copyright.
  * Update build dependencies.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Add patch to add hardening flags to the build.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Feb 2020 23:00:16 +0100

libsyntax-keyword-dynamically-perl (0.01-2) unstable; urgency=medium

  * Source-only no-change re-upload.

 -- gregor herrmann <gregoa@debian.org>  Thu, 26 Dec 2019 20:41:15 +0100

libsyntax-keyword-dynamically-perl (0.01-1) unstable; urgency=low

  * Initial release (closes: #946701).

 -- gregor herrmann <gregoa@debian.org>  Sat, 14 Dec 2019 02:20:05 +0100
